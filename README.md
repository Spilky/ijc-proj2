﻿# Jazyk C - 2. projekt #


## Hodnocení ##


6/10


## Chyby ##

[C1]    [tail.c:124] Chybné použití fgetc()
           * fgetc() vrací int, nikoliv char, jeho výsledek tak nelze uložit
             do proměnné typu char
[C1]    [wordcount.c:33] Memory leak (s)
[Z1]    [tail,tail2] Nesprávná validace argumentu -n: (pokud chybí, není číslo atd.)
  * Očekávané:  Chybová hláška, návratový kód není 0
  * Pozorované: RC=0, výstup (tail -n + FILE)
[Z1]    Prototyp 'htable_foreach' neodpovídá zadání
[C2]    [htable_clear] Po provedení je tabulka v nekonzistentním stavu
  * Nefunguje korektně: ukazatel v poli není nikdy nastaven na NULL, takže
    po rušení položek zůstávají v tabulce dangling pointery