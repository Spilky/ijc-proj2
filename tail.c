// tail.c
// Řešemí IJC-DU1, příklad a), 4.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Vytiskutí posledních počet řádků ze souboru. Oba parametry zadává uživatel.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define PARM_ERR 1      //chybnì zadaný parametr
#define FOPEN_ERR 2     //cybně otevřený soubor
#define FCLOSE_ERR 3    //chybně zavřený soubor

#define KOLIK_RADKU 1
#define OD_RADKU 2

#define MAX_CHARS 1024;

void printErr();
int PocetRadku();
void tail();

void printErr(int err)
{
  switch(err) {
    case PARM_ERR:
      fprintf(stderr, "\nChyba v zadani parametru!\n");
      exit (1);
      break;
    case FOPEN_ERR:
      fprintf(stderr, "\nChyba v otevreni souboru!\n");
      exit (1);
      break;
    case FCLOSE_ERR:
      fprintf(stderr, "\nChyba v zavreni souboru!\n");
      break;
    default:
      fprintf(stderr, "\nNeznámá chyba! Vse spatne!\n");
      exit (1);
      break;
  }
  return;
}

FILE * fileOpen(char soubor[], bool *file)
{
    FILE *vstup;

    if ((vstup = fopen(soubor, "r")) == NULL)
    {
        printErr(FOPEN_ERR);
    }
    *file = true;
    return vstup;
}

int PocetRadku(FILE *fr)
{
    int c;
    int pocet = 1;

    while((c = getc(fr)) != EOF)
    {
        if (c == '\n')
            pocet++;
    }
    fseek(fr, 0, SEEK_SET);
    return pocet;
}

void tail(FILE *vstup, long int od, long int kolik, int funkce)
{
    char odpad;
    int i = 1;
    int pocet = PocetRadku(vstup);

    if(od <= 0)
    {
        od = 1;
    }

    if(funkce == OD_RADKU)
    {
        kolik = pocet - od;
        if(kolik < 1)
            return;
        while(i < od && (odpad = getc(vstup)) != EOF)
        {
            if (odpad == '\n')
                i++;
        }
    }
    else if(funkce == KOLIK_RADKU)
    {
        od = pocet - kolik;
        if(od < 1)
            od = 1;

        while(i < od && (odpad = getc(vstup)) != EOF)
        {
            if (odpad == '\n')
                i++;
        }
    }

    int znaky = MAX_CHARS;
    char vystup[kolik][znaky + 1];
    int precteno;
    bool dlouhy = false;

    for(i = 0, precteno = 0; i < kolik; i++, precteno++)
    {
        if((fgets(vystup[i],znaky + 1,vstup)) == 0)
        {
            break;
        }
        if (strchr(vystup[i], '\n') == NULL)
        {
            dlouhy = true;
            vystup[i][znaky - 1] = '\n';
            while ((odpad = getc(vstup)) != '\n' && odpad != EOF);
        }
    }

    if(dlouhy)
    {
        fprintf(stderr, "Prilis dlouhy radek, orezavam\n");
    }

    for(i = 0; i < precteno; i++)
    {
        printf("%s", vystup[i]);
    }
}

int main (int argc, char *argv[])
{
    FILE *vstup = stdin;
    long int od_radku = 1;
    long int kolik_radku = 10;
    int funkce = KOLIK_RADKU;
    bool file = false;

    if (argc == 2)
    {
        vstup = fileOpen(argv[1], &file);
    }
    else if (argc == 3 || argc == 4)
    {
        if (strcmp("-n", argv[1]) == 0)
        {
            if (isdigit(argv[2][0]) != 0)
            {
                funkce = KOLIK_RADKU;
                char *pEnd;
                kolik_radku = strtol(argv[2],&pEnd,10);
                if (*pEnd != '\n' && *pEnd != 0)
                {
                    printErr(PARM_ERR);
                }
            }
            else if (argv[2][0] == '+')
            {
                funkce = OD_RADKU;
                char *pEnd;
                od_radku = strtol(&argv[2][1],&pEnd,10);
                if ((*pEnd != '\n' && *pEnd != 0) || (od_radku < 0))
                {
                    printErr(PARM_ERR);
                }
            }
            else
            {
                printErr(PARM_ERR);
            }
        }
        else
        {
            printErr(PARM_ERR);
        }

        if (argc == 4)
        {
            vstup = fileOpen(argv[3], &file);
        }
    }
    tail(vstup,od_radku,kolik_radku,funkce);
    if(file)
    {
        fclose(vstup);
    }
}
