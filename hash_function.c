// hash_function.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Hashovací funkce

unsigned int hash_function(const char *str, unsigned htable_size)
{
    unsigned int h=0;
    unsigned char *p;
    for(p=(unsigned char*)str; *p!='\0'; p++)
        h = 31*h + *p;
    return h % htable_size;
}
