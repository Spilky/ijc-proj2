// htable.h
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Hlavičkový soubor pro zdrojový kód v wordcount.c

#include "hash_function.h"

struct htable_listitem {
    char *key;
    int data;
    struct htable_listitem *next;
};

typedef struct htable {
    long htable_size;
    struct htable_listitem *itemsArray[];
} htable_t;

//Prototyp funkce pro vytvoření a inicializaci tabulky
htable_t * htable_init(long size);

//Prototyp funkce pro vyhledávání, či přidávání záznamů do tabulky
struct htable_listitem * htable_lookup(htable_t *t, const char *key);

//Prototyp funkce, která provede zadanou funkci nad každým prvkem tabulky
void htable_foreach(htable_t *t, void (*function)(struct htable_listitem *item));

//Prototyp funkce pro vyhledávání a rušení položky v tabulce
void htable_remove(htable_t *t, const char *key);

//Prototyp funkce pro zrušení všech položek v tabulce
void htable_clear(htable_t *t);

//Prototyp funkce pro zrušení tabulky (volá clear)
void htable_free(htable_t *t);

//Prototyp funkce pro vypís statistik tabulky
void htable_statistics(htable_t *t);

