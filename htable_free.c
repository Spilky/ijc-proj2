// htable_free.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro zrušení tabulky (volá funkci clear)

#include <stdlib.h>
#include "htable.h"

void htable_free(htable_t *t)
{
    htable_clear(t);
    free(t);

    return;
}
