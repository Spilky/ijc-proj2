// htable_foreach.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro provedení zadané funkce nad každým prvkem tabulky

#include <stdlib.h>
#include "htable.h"

void htable_foreach(htable_t *t, void (*function)(struct htable_listitem *item))
{
    struct htable_listitem *element = NULL;
    long i;

    //Procházím hahovací tabulku, prvek po prvku
    for(i = 0; i < t->htable_size; i++)
    {
        element = t->itemsArray[i];

        while(element != NULL)
        {
            function(element); //volání zadané funkce
            element = element->next;
        }
    }
}
