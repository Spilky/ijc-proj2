// tail2.cc
// Řešemí IJC-DU1, příklad a), 4.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Vytiskutí posledních počet řádků ze souboru. Oba parametry zadává uživatel.

#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <cstring>
#include <cstdlib>


const int PARM_ERR = 1;      //chybnì zadaný parametr
const int FOPEN_ERR = 2;     //cybně otevřený soubor
const int FCLOSE_ERR = 3;    //chybně zavřený soubor

const int KOLIK_RADKU = 1;
const int OD_RADKU = 2;

using namespace std;

void tail();
void printErr();

void printErr(int err)
{
  switch(err) {
    case PARM_ERR:
    {
       cerr << "\nChyba v zadani parametru!\n";
      exit (1);
      break;
    }
    case FOPEN_ERR:
    {
      cerr << "\nChyba v otevreni souboru!\n";
      exit (1);
      break;
    }
    case FCLOSE_ERR:
    {
      cerr << "\nChyba v zavreni souboru!\n";
     break;
    }
    default:
    {
      cerr << "\nNeznámá chyba! Vse spatne!\n";
      exit (1);
      break;
    }
  }
  return;
}

void tail(istream &vstup, long int od, long int kolik, int funkce)
{
    string str;
    deque<string> buffer;
    for(int i = 1; getline(vstup, str); i++)
    {
        if(funkce == OD_RADKU && i >= od)
        {
            cout<<str<<endl;
        }
        else
        {
            buffer.push_front(str);
            if (i > kolik)
            {
                 buffer.pop_back();
            }
        }
    }

    if(funkce == KOLIK_RADKU)
    {
        while(!buffer.empty())
        {
            cout<<buffer.back()<<endl;
            buffer.pop_back();
        }
    }
}

int main (int argc, char *argv[])
{
    istream *vstupStd = &cin;
    fstream vstupFile;
    bool file = false;
    long int od_radku = 1;
    long int kolik_radku = 10;
    int funkce = KOLIK_RADKU;

    if (argc == 2)
    {
        vstupFile.open(argv[1], fstream::in);
        if(!vstupFile.is_open())
        {
            printErr(FOPEN_ERR);
        }
        file = true;
    }
    else if (argc == 3 || argc == 4)
    {
        if (strcmp("-n", argv[1]) == 0)
        {
            if (isdigit(argv[2][0]) != 0)
            {
                funkce = KOLIK_RADKU;
                char *pEnd;
                kolik_radku = strtol(argv[2],&pEnd,10);
                if (*pEnd != '\n' && *pEnd != 0)
                {
                    printErr(PARM_ERR);
                }
            }
            else if (argv[2][0] == '+')
            {
                funkce = OD_RADKU;
                char *pEnd;
                od_radku = strtol(&argv[2][1],&pEnd,10);
                if ((*pEnd != '\n' && *pEnd != 0) || (od_radku < 0))
                {
                    printErr(PARM_ERR);
                }
            }
            else
            {
                printErr(PARM_ERR);
            }
        }
        else
        {
            printErr(PARM_ERR);
        }

        if (argc == 4)
        {
            vstupFile.open(argv[3], fstream::in);
            if(!vstupFile.is_open())
            {
                printErr(FOPEN_ERR);
            }
            file = true;
        }
    }

    if(file)
    {
        tail(vstupFile,od_radku,kolik_radku,funkce);
        vstupFile.close();
    }
    else
    {
        tail(*vstupStd,od_radku,kolik_radku,funkce);
    }
    return 0;
}
