// htable_remove.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro vyhledávání odstranění prvku v tabulce

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "htable.h"

void htable_remove(htable_t *t, const char *key)
{
    long index;

    //získání hashovací klíče
    index = hash_function(key, t->htable_size);

    struct htable_listitem *element = t->itemsArray[index];
    struct htable_listitem *prev_element = NULL;
    int result = -1;

    //nalezení záznamu
    while(element != NULL && (result = strcmp(key,element->key)) != 0)
    {
        prev_element = element;
        element = element->next;
    }

    //pokud jsme nalezli záznam
    if(result == 0)
    {
        //Když je prvek na konci řádku
        if((element->next == NULL) && (prev_element != NULL))
        {
            prev_element->next = NULL;
        }
        //Když je prvek v místě, kde je z obo stran obklopen dalšími prvky
        else if((element->next != NULL) && (prev_element != NULL))
        {
            prev_element->next = element->next;
        }
        //Když je prvek na začátku řádku a za ním následují další
        else if((element->next != NULL) && (prev_element == NULL))
        {
            t->itemsArray[index] = element->next;
        }
        //Kdž je prvek na začátku řádku a je tam sám
        else
        {
            t->itemsArray[index] = NULL;
        }
        free(element->key);
        free(element);
    }
    //pokud jsme nenalezli záznám, vypíšeme chybu
    else
    {
        fprintf(stderr, "Zaznam, ktery se pokousite odstaranit se v tabulce nenachazi");
    }
    return;
}
