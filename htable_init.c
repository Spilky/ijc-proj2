// htable_init.h
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro vytvoření a inicializaci hashovací tabulky

#include <stdlib.h>
#include <stdio.h>
#include "htable.h"

htable_t * htable_init(long size)
{
    long i;
    htable_t *hashTable;

    //alokace tabulky
    hashTable = (htable_t *) malloc(sizeof(htable_t) + size * sizeof(struct htable_listitem *));

    //pokud se alokace nezdaří, tak vrať NULL
    if( hashTable == NULL )
    {
        return NULL;
    }

    //uložení velikosti tabulky
    hashTable->htable_size = size;

    //vynulování ukazatelů
    for(i = 0; i < size; i++)
    {
        hashTable->itemsArray[i] = NULL;
    }

    return hashTable;
}
