// io.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Hlavičkový soubor funkce, která načte jedno slovo ze souboru

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int fgetword(char *s, int max, FILE *f);
