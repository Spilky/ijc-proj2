// htable_statistics.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Výpis statistik hashovací tabulky

#include <stdio.h>
#include "htable.h"

void htable_statistics(htable_t *t)
{
    struct htable_listitem *element = NULL;
    long i, pom;
    long max = 0;
    long min = 0;
    long soucet = 0;

    //Procházím hashovací tabulku prvek po prvku
    for(i = 0; i < t->htable_size; i++)
    {
        element = t->itemsArray[i];
        pom = 0;

        //Dokud nedojedu na konec řádku, počítám záznamy
        while(element != NULL)
        {
            pom++;
            element = element->next;
        }

        //Minumu přiřadím počet záznamů v prvním řádku
        if(i==0)
        {
            min = pom;
        }

        //Pokud je počet záznamů v aktuálním řádku vyšší, jak maximum, tak maximum = počet záznamů
        if(pom > max)
        {
            max = pom;
        }
        //Pokud je počet záznamů v aktuálním řádku nižší, jak minimum, tak minimum = počet záznamů
        else if(pom < min)
        {
            min = pom;
        }

        //Počítání celkového počtu záznamů
        soucet = soucet + pom;
    }

    //Tisk statistik
    printf("\nMin: %ld\tMax: %ld\tPrumer: %ld\n", min, max, soucet/t->htable_size);
    return;
}
