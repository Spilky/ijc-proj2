// io.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Funkce která načte jedno slovo ze souboru

#include "io.h"

int fgetword(char *s, int max, FILE *f)
{
    int c;
    int count;

    if(f == NULL)
    {
        fprintf(stderr,"Neplatný soubor");
        return EOF;
    }

    //Přeskočení bílých zanků na záčátku souboru
    while ( (c=fgetc(f)) != EOF && isspace(c)) ;

    //Pokud se narazí na EOF, tak skončí
    if (c == EOF)
        return EOF;

    //Zápis prvního znaku do řětězce
    s[0] = c;
    int i;
    for (count = 1, i = 1; isspace((c = fgetc(f))) == 0; count++, i++)
    {
        //Pokud je počet znaků větší jak maximum, tak již znaky nepřidávám
        //do řetězce, ale počet zanků počítám dále. Znaky po max přeskočím
        if (i >= max)
        {
            while (isspace(getc(f)) == 0)
                count++;
            break;
        }

        //Pokud narazím na konec souboru, tak ukončím řetězec a skončím
        if (c == EOF)
        {
            s[i] = '\0';
            return EOF;
        }

        //Načtení znaku do řetězce
        s[i] = c;
    }

    //Přidání ukončovacího znaku řetězce
    s[i] = '\0';

    return count;
}
