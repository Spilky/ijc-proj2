// htable_clear.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro provedení zadané funkce nad každým prvkem tabulky

#include <stdlib.h>
#include "htable.h"

void htable_clear(htable_t *t)
{
    struct htable_listitem *element = NULL;
    struct htable_listitem *next_element = NULL;
    long i;

    for(i = 0; i < t->htable_size; i++)
    {
        element = t->itemsArray[i]; //přiřadím elementu ukazatel na začátek řádku

        while(element != NULL) //procházím řádek a uvolňuji alkovanou paměť
        {
            next_element = element->next;
            free(element->key);
            free(element);
            element = next_element;
        }
    }
}
