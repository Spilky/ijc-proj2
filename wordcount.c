// io.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Počítání výskytu slov v souboru

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "io.h"
#include "htable.h"

const int MAX_CHARS = 255;
//Velikost tabulky je zvolena jako kompromis mezi rychlostí ve vyhledávání a potřebnou pamětí
const long TAB_SIZE = 1500;

void printElement(struct htable_listitem *element);

int main( void )
{
    bool long_word = false;
    char *s = NULL;
    s = malloc((MAX_CHARS + 1)*sizeof(char));
    int i;

    htable_t *table = htable_init(TAB_SIZE);

    if(table == NULL)
    {
        fprintf(stderr,"Nepodarilo se naalokovat pamet pro tabulku");
        return EXIT_FAILURE;
    }

    //Načítám slova s stdin, dokud nenarazíme na EOF
    while((i = fgetword(s, MAX_CHARS, stdin)) != EOF)
    {
        //Pokud je slovo delší jak MAX a ještě nebyla vypsána chybová hláška, vypiš ji
        if(i > MAX_CHARS && !long_word)
        {
            long_word = true;
            fprintf(stderr, "Jedno nebo vice slov bylo delsi jak %d znaku, takze bylo zkraceno\n", MAX_CHARS);
        }

        //Nepodařilo se přidat prvek do tabulky, zakřič, uvolni paměť a skonči
        if(htable_lookup(table, s) == NULL)
        {
            fprintf(stderr,"Nepodarilo se naalokovat pamet pro tabulku");
            htable_free(table);
            return EXIT_FAILURE;
        }
    }

    //Na každý prvek tabulky aplikuj funkci printElement
    htable_foreach(table, printElement);

    //Následující funkce by vypsala statistiky o tabulce
    //htable_statistics(table);

    //Uvolnění paměti
    htable_free(table);

    free(s);

    return EXIT_SUCCESS;
}

void printElement(struct htable_listitem *element)
{
    printf("%s\t%d\n", element->key, element->data);
    return;
}
