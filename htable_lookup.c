// htable_lookup.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul funkce pro vyhledávání v tabulce a přidávání prvků do tabulky

#include <stdlib.h>
#include <string.h>
#include "htable.h"

struct htable_listitem * htable_lookup(htable_t *t, const char *key)
{
    long index;

    //získání hashovací klíče
    index = hash_function(key, t->htable_size);

    struct htable_listitem *element = t->itemsArray[index];
    struct htable_listitem *prev_element = NULL;
    int result = -1;

    //Dokud nenajdu schodu v řetězcích nebo nedojdu na konec řádku, tak procházím řádek
    while(element != NULL && (result = strcmp(element->key,key)) != 0)
    {
        prev_element = element;
        element = element->next;
    }

    //Pokud naleznu řetězec v daném řádku, tak zvednu počet výskytů a vrátím ukazatel na prvek
    if(result == 0)
    {
        element->data++;
        return element;
    }
    //Pokud nenaleznu prvek v řádku, musím přidat nový na konec seznamu
    else
    {
        //Alokace paměti pro nový prvek
        struct htable_listitem *elementNew = NULL;
        elementNew = malloc(sizeof(struct htable_listitem));
        if(elementNew == NULL)
        {
            return NULL;
        }
        elementNew->key = malloc(sizeof(char) * (strlen(key) + 1));
        if(elementNew->key == NULL)
        {
            return NULL;
        }
        //Konec alokace paměti pro nový prvek

        //Přidání hodnot novému prvku
        elementNew->next = NULL;
        elementNew->data = 1;
        strcpy(elementNew->key, key);

        //Zařazení prvku na správné místo seznamu
        if(prev_element == NULL)
        {
            t->itemsArray[index] = elementNew;
        }
        else
        {
            prev_element->next = elementNew;
        }

        //Vrátím ukazatel na nový prvek
        return elementNew;
    }
}
