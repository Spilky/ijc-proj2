# Makefile
# Řešemí IJC-DU2, příklad a) b), 7.4.2013
# Autor: David Spika (xspilk00), FIT
# Makefile k DU2 do předmětu IJC (Jazyk C)

CFLAGS = -std=c99 -pedantic -Wall
CCFLAGS = -std=c++11 -pedantic -Wall

all: tail tail2 wordcount wordcount-dynamic

#TAIL and TAIL2

#sestavení programů tail a tail2

tail: tail.c
	gcc $(CFLAGS) -o tail tail.c

tail2: tail2.cc
	g++ $(CCFLAGS) -o $@ $<

#WORDCOUNT and WORDCOUNT-DYNAMIC

#sestavení linkovacích souborů pro knihovny

hash_function.o: hash_function.c hash_function.h
	gcc $(CFLAGS) -c -fPIC hash_function.c -o hash_function.o

htable_clear.o: htable_clear.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_clear.c -o htable_clear.o

htable_foreach.o: htable_foreach.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_foreach.c -o htable_foreach.o

htable_free.o: htable_free.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_free.c -o htable_free.o

htable_init.o: htable_init.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_init.c -o htable_init.o

htable_lookup.o: htable_lookup.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_lookup.c -o htable_lookup.o

htable_remove.o: htable_remove.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_remove.c -o htable_remove.o

htable_statistics.o: htable_statistics.c htable.h
	gcc $(CFLAGS) -c -fPIC htable_statistics.c -o htable_statistics.o

#sestavení linkovacích souborů, které nepatří do knihoven

io.o: io.c io.h
	gcc $(CFLAGS) -c io.c -o io.o

wordcount.o: wordcount.c htable.h
	gcc $(CFLAGS) -c wordcount.c -o wordcount.o

#sestavení knihoven

libhtable.a: hash_function.o htable_clear.o htable_foreach.o htable_free.o htable_init.o htable_lookup.o htable_remove.o htable_statistics.o htable.h
	ar rcs libhtable.a hash_function.o htable_clear.o htable_foreach.o htable_free.o htable_init.o htable_lookup.o htable_remove.o htable_statistics.o

libhtable.so: hash_function.o htable_clear.o htable_foreach.o htable_free.o htable_init.o htable_lookup.o htable_remove.o htable_statistics.o htable.h
	gcc -fPIC -shared hash_function.o htable_clear.o htable_foreach.o htable_free.o htable_init.o htable_lookup.o htable_remove.o htable_statistics.o -o libhtable.so

#sestavení programů wordcount a wordcount-dynamic

wordcount: io.o wordcount.o libhtable.a
	gcc $(CFLAGS) io.o wordcount.o libhtable.a -o wordcount

wordcount-dynamic: io.o wordcount.o libhtable.so
	gcc $(CFLAGS) -dynamic io.o wordcount.o libhtable.so -o wordcount-dynamic

#UKLÍZENÍ BORDELU

clean:
	rm -f *.o

clean-all: clean
	rm -f tail
	rm -f wordcount
	rm -f wordcount-dynamic
	rm -f tail2
	rm -f libhtable.a
	rm -f libhtable.so
