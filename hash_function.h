// hash_function.c
// Řešemí IJC-DU2, příklad b), 7.4.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Hlavičkový soubor pro zdrojový kód v hash_function.c

unsigned int hash_function(const char *str, unsigned htable_size);
